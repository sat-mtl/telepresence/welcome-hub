Release Notes
===================

welcome-hub 0.1.0 (2023-02-21)
---------------------------------

 * ♻️ Refactor the EventsPage component to adapt to the new UI changes ([!20](https://gitlab.com/sat-mtl/tools/scenic/scenic-light/welcome-hub/-/merge_requests/20))
 * ✨ Implement the CreateEventForm component (partially) ([!21](https://gitlab.com/sat-mtl/tools/scenic/scenic-light/welcome-hub/-/merge_requests/21))
 * ✨ Implement the invite partners form ([!18](https://gitlab.com/sat-mtl/tools/scenic/scenic-light/welcome-hub/-/merge_requests/18))
 * 📦️ Add ui-components package with v.0.2 ([!17](https://gitlab.com/sat-mtl/tools/scenic/scenic-light/welcome-hub/-/merge_requests/17))
 * ✅ Add remaining unit tests ([!16](https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub/-/merge_requests/16))
 * ✅ Setup a testing environment and add unit tests ([!15](https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub/-/merge_requests/15))
 * 🌐 Add an option to translate all UI in english and in french ([!14](https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub/-/merge_requests/14))
 * 🔖 Display the version and the hash commit in the UI ([!12](https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub/-/merge_requests/12))
 * ⬆️ Update ui-components dependency ([!13](https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub/-/merge_requests/13))

welcome-hub 0.0.0 (2022-04-13)
----------------------------------

* ✨ Implement the ItemCollection component
* ✨ Implement the side bar component
* ✨ Implement the ContactTable
* 💄 Make cursor style pointer for items in an item collection
* 🍱 Add the models assets in JSON
* 👷 Add s3 bucket deployment
* 💻 Add Feature Request and Bug Resolution Request templates
* ⚗ Import a ui-components component in order to bootstrap this project