with import <nixpkgs> {};

let
  jsPkgs = with nodePackages; [
    npm-check-updates
  ];

in mkShell {
  name = "webapp-dev";

  nativeBuildInput = [
    binutils gcc gnumake openssl pkgconfig # common deps
  ];

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    git less vim # common tools
    nodejs-16_x # js tools
    jsPkgs
    entr
  ];

  shellHook = ''
    echo "Using NodeJS $(node --version)"
  '';
}
