import React, { createContext } from 'react'
import { useTranslation } from 'react-i18next'
import PageLayout from '@components/PageLayout'

import '@styles/App.scss'
import { Context, i18nConfig, Shared } from '@sat-mtl/ui-components'

// @todo Debug ui-components css, it triggers the heap error
import '@sat-mtl/ui-components/ui-components.css'
import populateStores from '~/src/populateStores'
import LocalesEn from '@assets/locales/en/locale.json'
import LocalesFr from '@assets/locales/fr/locale.json'

import NpmPackage from '~/package.json'

const { ThemeProvider } = Context
const { VersionSection } = Shared

/**
 * @constant {external:react/Context} AppStoresContext - Dispatches all the App stores
 * @memberof components.App
 */
export const AppStoresContext = createContext({})

/**
 * @constant {string} repoLink - The link to the project's repository
 * @memberof components.App
 */
export const repoLink = 'https://gitlab.com/sat-mtl/telepresence/scenic-light/welcome-hub'

const stores = populateStores()

/**
 * Provides the i18n configuration to the app
 * @param {json} LocalesEn - The json object for en translations
 * @parma {json} LocalesFr - The json object for fr translations
 * @param {?string} - The default language to use when a chosen language is not available
 * @param {?string} - The language param that is used to changed the language, if required
 * @returns {external:ui-components/src} - The configuration object for i18n
 */
i18nConfig(LocalesEn, LocalesFr, 'en', 'lang')

/**
* Renders the App
*/
export default function App () {
  const { t } = useTranslation()
  const title = t('Software version')

  return (
    <ThemeProvider value='gabrielle'>
      <AppStoresContext.Provider value={stores}>
        <PageLayout />
        <VersionSection NpmPackage={NpmPackage} repoLink={repoLink} title={title} />
      </AppStoresContext.Provider>
    </ThemeProvider>
  )
}
