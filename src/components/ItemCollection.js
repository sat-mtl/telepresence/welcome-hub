import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { v4 as uuidv4 } from 'uuid'
import { Link } from 'react-router-dom'
import { Common } from '@sat-mtl/ui-components'

import { AppStoresContext } from '@components/App'
import { RouteEnum } from '@models/RouteEnum'
import { CreateEventEnum } from '@models/CreateEventCycleEnum'
import { formatDateValue } from '@utils/helperFunctions'

import '@styles/ItemCollection.scss'

const { Button, Icon, Tag } = Common

/**
 * Renders the header of an item collection
 * @selector `.ItemHeader`
 * @param {string} preview - The link to the image that should be displayed as a thumbnail for the item
 * @param {string} title - The title of the item
 * @param {string} date - The date of the item
 * @returns {external:react/Component} The header of an item
 * @todo: get the img src as a string from preview in DB
 */
const ItemHeader = ({ preview, title, date, locations }) => {
  const { t } = useTranslation()

  return (
    <div className='ItemHeader'>
      {preview &&
        <div className='ItemThumbnail'>
          <img src={require(`~/assets/img/${preview}.png`)} alt='item' />
        </div>}
      <div className='title'>
        {locations && <span className='HallsNumber'>{`${locations} ${t('Halls')} - `}</span>}
        <span>{t(title)}</span>
        {date && <span className='date'>{date}</span>}
      </div>
    </div>
  )
}

/**
 * Renders the tags of an item
 * @selector `.TagsContainer`
 * @param {object} tags - An array containing all labels for the item's tags
 * @returns {external:react/Component} The tags container of an item
 */
export const TagsContainer = ({ tags }) => {
  const { t } = useTranslation()

  return (
    <div className='TagsContainer'>
      {tags?.map(tag => {
        return (
          <Tag key={uuidv4()}>{t(tag)}</Tag>
        )
      })}
    </div>
  )
}

/**
 * Renders the buttons of an item
 * @selector `.ButtonsContainer`
 * @param {string} id - The id of the preset item
 * @param {string} buttonLabel - The label of the rendered button
 * @returns {external:react/Component} The buttons container of an item
 */
export const ButtonsContainer = ({ id, buttonLabel }) => {
  const { createEventCycleStore, itemStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  return (
    <div className='ButtonsContainer'>
      {buttonLabel?.map(label => {
        return (
          <Link to={RouteEnum.CREATE_EVENT_FORM} key={uuidv4()}>
            <Button
              size='small'
              onClick={() => {
                createEventCycleStore.setCurrentCycle(CreateEventEnum.CREATING)
                itemStore.setCurrentItemId(id)
              }}
            >
              {t(label)}
            </Button>
          </Link>
        )
      })}
    </div>
  )
}

/**
 * Renders an item of a collection of items
 * @selector `#Item`
 * @param {string} preview - The link to the image that should be displayed as a thumbnail for the item
 * @param {string} title - The title of the item
 * @param {string} date - The date of the item
 * @param {string} description - The brief description of the item
 * @param {object} tags - An array containing all labels for the item's tags
 * @returns {external:react/Component} The item of a collection
 */
export const Item = ({ item }) => {
  const { id, preview, title, type, date, description, tags, locations, buttonLabel, detailsLabel } = item
  const { t } = useTranslation()

  return (
    <li id='Item' key={uuidv4()}>
      <ItemHeader preview={preview} title={title} date={date ? formatDateValue(date) : ''} locations={locations} />
      <div className='DescriptionContainer'>
        <span className='Description'>
          {t(description)}
          {locations && <span><span className='TitleBold'>{`${t('Locations')}`}</span>{`: ${locations}`}</span>}
          {type && <span><span className='TitleBold'>Type</span>{`: ${t(type)}`}</span>}
          {detailsLabel && <span className='MoreDetailsLink'><a href=''>{t('More details')}</a></span>}
        </span>
      </div>
      {tags?.length !== 0 && <TagsContainer tags={tags} />}
      {buttonLabel && <ButtonsContainer buttonLabel={buttonLabel} id={id} />}
    </li>
  )
}

/**
 * Renders a button that allows the addition of an item to the collection
 * @selector `#ItemCollectionAdder`
 * @returns {external:react/Component} The item collection adder button
 */
export const ItemCollectionAdder = () => {
  return (
    <div className='ItemCollectionAdder'>
      <Button variant='outlined' size='normal'>
        <Icon type='plus' />
      </Button>
    </div>
  )
}

/**
 * Renders an items collection
 * @selector `#ItemCollection`
 * @param {object} items - An array containing a number of items
 * @param {function} onNew - A function called when the adder button in clicked
 * @returns {external:react/Component} The items collection of a page
 */
const ItemCollection = ({ items, onNew }) => {
  return (
    <div id='ItemCollection'>
      <ul>
        {items.map(item => {
          return (
            <Item
              key={uuidv4()}
              item={item}
            />
          )
        })}
      </ul>
      {onNew && <ItemCollectionAdder onClick={onNew} />}
    </div>
  )
}

export default ItemCollection
