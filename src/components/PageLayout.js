import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { Route, Routes } from 'react-router-dom'

import { AppStoresContext } from '@components/App'
import WelcomeHeader from '@components/WelcomeHeader'
import SideBarMenu from '@components/SideBarMenu/SideBarMenu'
import HomePage from '@components/pages/HomePage'
import TrainingPage from '@components/pages/TrainingPage'
import ContactsPage from '@components/pages/ContactsPage'
import CreateEventPage from '@components/pages/CreateEventPage'
import EventsPage from '@components/pages/EventsPage'
import { Context } from '@sat-mtl/ui-components'

import { RouteEnum } from '@models/RouteEnum'
import InvitePartnersForm from '@components/forms/InvitePartnersForm'
import CreateEventForm from '@components/forms/CreateEventForm'

const { ThemeContext } = Context

/**
 * Renders the layout of all list pages with a router
 * @selector `#PageLayout`
 * @returns {external:react/Component} The page layout router
 */
const PageLayout = observer(() => {
  const theme = useContext(ThemeContext)
  const { createEventCycleStore, eventsStore } = useContext(AppStoresContext)

  return (
    <div id='PageLayout' className={`Page-${theme}`}>
      <WelcomeHeader />
      <main>
        {!createEventCycleStore.isInviting && !createEventCycleStore.isCreating && <SideBarMenu />}
        <Routes>
          <Route path={RouteEnum.HOME} exact element={<HomePage />} />
          <Route path={RouteEnum.TRAINING} element={<TrainingPage />} />
          <Route path={RouteEnum.CONTACTS} element={<ContactsPage />} />
          <Route path={RouteEnum.CREATE_EVENT} element={<CreateEventPage />} />
          <Route path={RouteEnum.EVENTS} element={<EventsPage events={eventsStore.availableEvents} />} />
          <Route path={RouteEnum.CREATE_EVENT_FORM} element={<CreateEventForm />} />
          <Route path={RouteEnum.INVITE_PARTNERS_FORM} element={<InvitePartnersForm />} />
          <Route path='*' element={<HomePage />} />
        </Routes>
      </main>
    </div>
  )
})

export default PageLayout
