import React from 'react'
import { createUseStyles } from 'react-jss'

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  LocationPin: {
    backgroundColor: props => props.color,
    borderRadius: '100%',
    height: '.8rem',
    width: '.8rem',
    marginRight: '.5rem'
  },
  SceneSchemaLocation: {
    display: 'flex',
    alignItems: 'center',
    color: '#FFFFFF',
    fontWeight: '700'
  }
})

/** Renders a scene schema location
 * @param {Object} props - Props that are passed to the react component
 * @returns {external:react/Component} The scene schema location
*/
const SceneSchemaLocation = (props) => {
  const classes = useStyles(props)
  return (
    <div className={`${classes.SceneSchemaLocation} SceneSchemaLocation`}>
      <div className={`${classes.LocationPin} LocationPin`} />
      {props.location}
    </div>
  )
}

export default SceneSchemaLocation
