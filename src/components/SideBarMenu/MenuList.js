export const MainMenuList = [
  {
    title: 'Home',
    url: '/',
    iconType: 'home'
  },
  {
    title: 'Training',
    url: '/training-page',
    iconType: 'lightbulb'
  },
  {
    title: 'Contacts',
    url: '/contacts-page',
    iconType: 'contactsList'
  }
]

export const EventsMenuList = [
  {
    title: 'Create an event',
    url: '/create-event-page',
    iconType: 'lightbulb'
  },
  {
    title: 'Events',
    url: '/events-page',
    iconType: 'events'
  }
]
