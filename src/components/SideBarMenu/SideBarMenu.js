import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import { NavLink } from 'react-router-dom'
import { MainMenuList, EventsMenuList } from './MenuList'

import { Common } from '@sat-mtl/ui-components'
import '@styles/SideBarMenu.scss'
import { useTranslation } from 'react-i18next'

const { Icon } = Common

/**
 * Renders an item of the sidebar menu
 * @selector `#SideMenuItem${-selected}`
 * @returns {external:react/Component} The sidebar menu item
 */
export const MenuItem = ({ item }) => {
  const { t } = useTranslation()
  const iconStyles = { width: '1rem', height: '1.5rem', transform: 'translateY(5px)', marginLeft: '1rem' }
  return (
    <NavLink
      to={item.url} className={({ isActive }) =>
        !isActive ? 'SideMenuItem' : 'SideMenuItem-selected'}
    >
      <div style={iconStyles}>
        <Icon type={item.iconType} />
      </div>
      <span>{t(item.title)}</span>
    </NavLink>
  )
}

/**
 * Renders the sidebar menu of the welcome hub pages
 * @selector `#SideBarMenu`
 * @returns {external:react/Component} The sidebar navigation menu
 */
const SideBarMenu = () => {
  return (
    <nav id='SideBarMenu'>
      <ul>
        {MainMenuList.map(item => <MenuItem item={item} key={uuidv4()} />)}
      </ul>

      <h4>Events</h4>
      <ul>
        {EventsMenuList.map(item => <MenuItem item={item} key={uuidv4()} />)}
      </ul>
    </nav>
  )
}

export default SideBarMenu
