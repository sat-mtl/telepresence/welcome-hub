import React from 'react'

/**
 * @constant {String} PARENT_APP - The name of the parent project of all micro apps
 * @memberof module:LoginHeader
 */
export const PARENT_APP = 'Scenic Light'

/**
 * Renders the header of the welcomeHub application
 * @selector `#WelcomeHeader`
 * @param {string} title - The title of the header
 * @returns {external:react/Component} The welcome hub header
 */
const WelcomeHeader = () => {
  return (
    <header id='WelcomeHeader'>
      <div className='Header'>
        <h3>{PARENT_APP}</h3>
      </div>
    </header>
  )
}

export default WelcomeHeader
