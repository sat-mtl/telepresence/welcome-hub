import React, { useContext, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { observer } from 'mobx-react-lite'
import { Inputs } from '@sat-mtl/ui-components'

import { AppStoresContext } from '@components/App'
import { FormMetaHeader, FormHeader, FormFooter, FormError } from '@components/forms/FormUtils'

import { CreateEventEnum } from '@models/CreateEventCycleEnum'
import { RouteEnum } from '@models/RouteEnum'

import '@styles/form.scss'
import '@styles/CreateEventForm.scss'

const { InputText } = Inputs

/**
 * Makes the initial state object for the create event form
 * @returns {Object} initCreateEventFormState - The state object of the create event form
 */
const initCreateEventFormState = function () {
  return {
    eventName: '',
    eventDate: '',
    eventTime: '',
    isEventNameValid: false,
    isEventDateValid: false,
    isEventTimeValid: false
  }
}

/**
 * Renders the field for the meeting name
 * @selector`.InputContainer`
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @returns {external:react/Component} The hall name field of the create event form
 */
export const MeetingNameField = ({ credentials, setCredentials }) => {
  const { t } = useTranslation()
  const [isFieldFocused, setIsFieldFocused] = useState(false)
  const onFieldFocus = () => setIsFieldFocused(true)
  const onFieldBlur = () => setIsFieldFocused(false)

  const handleNameValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        eventName: value,
        isEventNameValid: value.length >= 4
      }
    })
  }

  return (
    <div className='InputContainer'>
      <InputText
        title={t('Name of the meeting')}
        placeholder={t('Enter the name of the meeting')}
        value={credentials.eventName}
        onChange={(e) => { handleNameValidation(e.target.value) }}
        size='large'
        onFocus={onFieldFocus}
        onBlur={onFieldBlur}
      />
      <FormError
        message='Enter a valid meeting name'
        visibility={!(isFieldFocused && !credentials.isEventNameValid) && 'hidden'}
      />
    </div>
  )
}

/**
 * Renders the field for the meeting date
 * @selector`.InputContainer`
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @returns {external:react/Component} The hall name field of the create event form
 * @todo: This component should render a calendarField when this one is implemented in ui-components
 */
export const MeetingDateField = ({ credentials, setCredentials }) => {
  const { t } = useTranslation()
  const [isFieldFocused, setIsFieldFocused] = useState(false)
  const onFieldFocus = () => setIsFieldFocused(true)
  const onFieldBlur = () => setIsFieldFocused(false)

  const handleDateValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        eventDate: value,
        // dummy validation
        isEventDateValid: value.length >= 4
      }
    })
  }

  return (
    <div className='InputContainer'>
      <InputText
        title={t('Date')}
        placeholder={t('Enter the date of the meeting')}
        value={credentials.eventDate}
        onChange={(e) => { handleDateValidation(e.target.value) }}
        size='large'
        onFocus={onFieldFocus}
        onBlur={onFieldBlur}
      />
      <FormError
        message='Enter a valid meeting date'
        visibility={!(isFieldFocused && !credentials.isEventDateValid) && 'hidden'}
      />
    </div>
  )
}

/**
 * Renders the field for the meeting time
 * @selector`.InputContainer`
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @returns {external:react/Component} The hall name field of the create event form
 * @todo: This component should render a select hour field
 */
export const MeetingTimeField = ({ credentials, setCredentials }) => {
  const { t } = useTranslation()
  const [isFieldFocused, setIsFieldFocused] = useState(false)
  const onFieldFocus = () => setIsFieldFocused(true)
  const onFieldBlur = () => setIsFieldFocused(false)

  const handleTimeValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        eventTime: value,
        // dummy validation
        isEventTimeValid: value.length >= 4
      }
    })
  }

  return (
    <div className='InputContainer'>
      <InputText
        title={t('Choose the time')}
        placeholder={t('Enter the time of the meeting')}
        value={credentials.eventTime}
        onChange={(e) => { handleTimeValidation(e.target.value) }}
        size='large'
        onFocus={onFieldFocus}
        onBlur={onFieldBlur}
      />
      <FormError
        message='Enter a valid meeting time'
        visibility={!(isFieldFocused && !credentials.isEventTimeValid) && 'hidden'}
      />
    </div>
  )
}

/**
 * Renders a container of required fields
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @returns {external:react/Component} The create event form fields container
 */
const FormContent = ({ credentials, setCredentials }) => {
  return (
    <div className='FormContentContainer'>
      <MeetingNameField
        credentials={credentials}
        setCredentials={setCredentials}
      />
      <MeetingDateField
        credentials={credentials}
        setCredentials={setCredentials}
      />
      <MeetingTimeField
        credentials={credentials}
        setCredentials={setCredentials}
      />
    </div>

  )
}

/**
 * Renders the create event form
 * @selector `.CreateEventForm`
 * @returns {external:mobx-react/ObserverComponent} The create event form of the app
 */
const CreateEventForm = observer(() => {
  const { createEventCycleStore } = useContext(AppStoresContext)
  const [credentials, setCredentials] = useState(initCreateEventFormState())

  const formTitleCaption = 'Identify your event by giving it a name and configure the date and time it will take place'
  const formTitle = 'Create an event'

  const onSubmitHandler = (event) => {
    event.preventDefault()
    const data = credentials
    console.log(data)
    // clear form
    setCredentials(initCreateEventFormState())
    createEventCycleStore.setCurrentCycle(CreateEventEnum.INVITING)
  }

  const isFormValid = credentials.isEventNameValid && credentials.isEventDateValid && credentials.isEventTimeValid

  return (
    <form className='CreateEventForm' onSubmit={onSubmitHandler}>
      <FormMetaHeader onCloseButtonHandler={() => createEventCycleStore.setCurrentCycle(CreateEventEnum.BROWSING)} />
      <FormHeader formTitle={formTitle} formTitleCaption={formTitleCaption} />
      <FormContent
        credentials={credentials}
        setCredentials={setCredentials}
      />
      <FormFooter
        sendButtonLabel='Next Step'
        sendButtonDisabled={!isFormValid}
        routePath={RouteEnum.INVITE_PARTNERS_FORM}
        onSubmitHandler={onSubmitHandler}
        onCancelHandler={() => { console.log('should close the form!!!') }}
      />
    </form>
  )
})

export default CreateEventForm
