import React from 'react'
import { useTranslation } from 'react-i18next'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'
import { Common } from '@sat-mtl/ui-components'

const { Button, Icon } = Common
/**
 * Renders the close button of the form
 * @param {function} onCloseButtonHandler - A function that is triggered when the close button is clicked
 * @returns {external:react/Component} The form's close button
 */
export const FormMetaHeader = ({ onCloseButtonHandler }) => {
  return (
    <div className='MetaHeader'>
      <Button
        id='CloseFormButton'
        size='tiny'
        shape='circle'
        variant='text'
        onClick={() => {}}
      >
        <Icon type='closeButton' onClick={onCloseButtonHandler} />
      </Button>
    </div>
  )
}

/**
 * Renders the form header
 * @param {string} formTitle - The title of the form
 * @param {string} [formTitleCaption=''] - A title caption
 * @returns {external:react/Component} The header of the form
 */
export const FormHeader = ({ formTitle, formTitleCaption = '' }) => {
  const { t } = useTranslation()
  return (
    <header>
      <div className='FormTitle'>
        <h4>{t(formTitle)}</h4>
      </div>
      <div className='FormTitleCaption'>
        <span>{t(formTitleCaption)}</span>
      </div>
    </header>
  )
}

/**
 * Renders the form footer
 * @selector`.FormFooter`
 * @param {function} onCancelHandler - A function that is triggered when the cancel button is clicked
 * @param {function} onSubmitHandler - A function that is triggered when the form is submitted
 * @param {string} routePath - A route path
 * @param {string} sendButtonLabel - The label of the send button
 * @param {boolean} sendButtonDisabled - Flags true when a given condition is met to disable the submit button
 * @returns {external:mobx-react/ObserverComponent} The form footer
 */
export const FormFooter = observer(({ sendButtonDisabled, sendButtonLabel, onSubmitHandler, onCancelHandler, routePath }) => {
  const { t } = useTranslation()
  const navigate = useNavigate()

  return (
    <footer className='FormFooter'>
      <Button
        id='CloseButton'
        size='large'
        shape='rectangle'
        variant='outlined'
        onClick={onCancelHandler}
      >
        {t('Close')}
      </Button>

      <Button
        id='SendButton'
        size='large'
        shape='rectangle'
        disabled={sendButtonDisabled}
        onClick={event => {
          onSubmitHandler(event)
          navigate(routePath)
        }}
      >
        {t(sendButtonLabel)}
      </Button>
    </footer>
  )
})

/**
 * Renders the error message of each form field
 * @param {string} visibility - Checks if the component should be visible or not
 * @param {string} message - The message to be displayed in the event of an error
 * @returns {external:react/Component} The form's error message component
 */
export const FormError = ({ visibility, message }) => {
  const { t } = useTranslation()
  return (
    <div
      className='FormError'
      style={{ visibility: visibility }}
    >
      {t(message)}
    </div>
  )
}
