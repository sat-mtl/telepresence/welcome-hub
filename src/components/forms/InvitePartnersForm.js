import React, { useContext, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { observer } from 'mobx-react-lite'

import { FormMetaHeader, FormHeader, FormFooter, FormError } from '@components/forms/FormUtils'
import InvitePartnerFormContainer from '@utils/InvitePartnerFormContainer'
import { areAllBoolFieldsValid } from '@utils/helperFunctions'
import { AppStoresContext } from '@components/App'
import { Inputs } from '@sat-mtl/ui-components'

import { CreateEventEnum } from '@models/CreateEventCycleEnum'

import '@styles/form.scss'
import { RouteEnum } from '@models/RouteEnum'

const { InputText } = Inputs

/**
 * @constant {RegExp} EMAIL_REGEX - Matches all valid email addresses forms
 * @memberof module:InvitePartnersForm
 */
const EMAIL_REGEX = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i

/**
 * @constant {RegExp} BOOL_PROP_REGEX - Matches all properties that need to be validated
 * @memberof module:InvitePartnersForm
 */
const BOOL_PROP_REGEX = /^is\w+Valid/

// For testing purposes
// @todo: This information should come from a preset json object that is attached to the respective preset
const formTitleCaption = 'Name the 3 participating rooms and invite your two partners to join this event. An invitation will be sent to them by e-mail'
const formTitle = 'Invite my partners'
const colors = ['#56C2F6', '#FDD153', '#DE30F6']

/**
 * Makes the initial state object for the invite partners form
 * @param {Object} halls - An array of the halls required for a given preset
 * @returns {Object} initPartnerFormState - The state object of the invite partners form
 */
const initPartnerFormState = function (halls) {
  const stateObj = {}
  halls?.forEach((_, i) => {
    stateObj[`name${i + 1}`] = ''
    stateObj[`email${i + 1}`] = ''
    stateObj[`isName${i + 1}Valid`] = ''
    stateObj[`isEmail${i + 1}Valid`] = ''
  })
  return stateObj
}

/**
 * Renders the Invite partners form hall name field
 * @selector`.InputContainer`
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @param {string} title - The title of the Hall name field
 * @param {number} i - The number associated to the hall
 * @returns {external:react/Component} The hall name field of the Invite partners form
 */
export const HallNameField = ({ credentials, setCredentials, title, hallNumber }) => {
  const { t } = useTranslation()
  const [isFieldFocused, setIsFieldFocused] = useState(false)
  const onFieldFocus = () => setIsFieldFocused(true)
  const onFieldBlur = () => setIsFieldFocused(false)

  const handleNameValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        [`name${hallNumber}`]: value,
        [`isName${hallNumber}Valid`]: value.length >= 3
      }
    })
  }

  return (
    <div className='InputContainer'>
      <InputText
        title={t(`Name of ${title}`)}
        placeholder={t('Enter the name of the hall')}
        value={credentials[`name${hallNumber}`]}
        onChange={(e) => { handleNameValidation(e.target.value) }}
        size='large'
        onFocus={onFieldFocus}
        onBlur={onFieldBlur}
      />
      <FormError
        message='Enter a valid name'
        visibility={!(isFieldFocused && !credentials[`isName${hallNumber}Valid`]) && 'hidden'}
      />
    </div>
  )
}

/**
 * Renders the Invite partners form email field
 * @selector`.InputContainer`
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @param {string} title - The title of the email field
 * @param {number} i - The number associated to the hall
 * @returns {external:react/Component} The email field of the Invite partners form
 */
export const EmailField = ({ credentials, setCredentials, title, hallNumber }) => {
  const { t } = useTranslation()
  const [isEmailFocused, setIsEmailFocused] = useState(false)
  const onEmailFocus = () => setIsEmailFocused(true)
  const onEmailBlur = () => setIsEmailFocused(false)

  const handleEmailValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        [`email${hallNumber}`]: value,
        [`isEmail${hallNumber}Valid`]: EMAIL_REGEX.test(value)
      }
    })
  }

  return (
    <div className='InputContainer'>
      <InputText
        title={t(`Email of ${title} organizer`)}
        placeholder={t('Enter an email address')}
        value={credentials[`email${hallNumber}`]}
        onChange={(e) => { handleEmailValidation(e.target.value) }}
        size='large'
        onFocus={onEmailFocus}
        onBlur={onEmailBlur}
      />
      <FormError
        message='Enter a valid email address'
        visibility={!(isEmailFocused && !credentials[`isEmail${hallNumber}Valid`]) && 'hidden'}
      />
    </div>
  )
}

/**
 * Renders a container of required fields
 * @param {Object} credentials - A state object
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @param {string} title - The title of the Hall name field
 * @param {number} hallNumber - The number associated to the hall
 * @param {string} color - The color of the fields container
 * @returns {external:react/Component} The Invite partners form of the app
 */
const FormContent = ({ credentials, setCredentials, title, hallNumber, color }) => {
  return (
    <InvitePartnerFormContainer hallNumber={hallNumber} title={title} color={color}>
      <HallNameField
        title={title}
        credentials={credentials}
        setCredentials={setCredentials}
        hallNumber={hallNumber}
      />
      <EmailField
        title={title}
        credentials={credentials}
        setCredentials={setCredentials}
        hallNumber={hallNumber}
      />
    </InvitePartnerFormContainer>

  )
}

/**
 * Renders the Invite partners form
 * @selector `.InvitePartnersForm`
 * @returns {external:mobx-react/ObserverComponent} The Invite partners form of the app
 */
const InvitePartnersForm = observer(() => {
  const { itemStore, createEventCycleStore } = useContext(AppStoresContext)
  const { currentPresetItem: { halls } } = itemStore
  const [credentials, setCredentials] = useState(initPartnerFormState(halls))

  const onSubmitHandler = (event) => {
    event.preventDefault()
    const data = credentials
    console.log(data)
    // clear form
    createEventCycleStore.setCurrentCycle(CreateEventEnum.BROWSING)
    setCredentials(initPartnerFormState(halls))
  }

  return (
    <form className='InvitePartnersForm'>
      <FormMetaHeader onCloseButtonHandler={() => createEventCycleStore.setCurrentCycle(CreateEventEnum.BROWSING)} />
      <FormHeader formTitle={formTitle} formTitleCaption={formTitleCaption} />
      {halls?.map((hall, i) => {
        return (
          <FormContent
            key={i}
            title={hall}
            credentials={credentials}
            setCredentials={setCredentials}
            hallNumber={i + 1}
            color={colors[i]}
          />
        )
      })}
      <FormFooter
        sendButtonLabel='Send'
        onSubmitHandler={onSubmitHandler}
        sendButtonDisabled={!areAllBoolFieldsValid(credentials, BOOL_PROP_REGEX)}
        routePath={RouteEnum.HOME}
        onCancelHandler={() => { console.log('should close the form!!!') }}
      />
    </form>
  )
})

export default InvitePartnersForm
