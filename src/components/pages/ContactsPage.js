import React, { useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'

import { ItemCollectionAdder } from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'
import classNames from 'classnames'

import '@styles/HomePage.scss'
import '@styles/ContactsPage.scss'

/**
 * Renders the header of the contacts list page
 * @selector `.ListHeader`
 * @returns {external:react/Component} The contacts page header
 */
export const ContactsPageHeader = () => {
  const { t } = useTranslation()

  return (
    <div className='ListHeader'>
      <h4>{t('My contacts')}</h4>
    </div>
  )
}

/**
 * Renders the header of the contacts list table
 * @selector `.ContactsTableHeader`
 * @returns {external:react/Component} The contacts table header
 */
export const ContactsTableHeader = () => {
  const { t } = useTranslation()

  return (
    <tr className='ContactsTableHeader'>
      <td>{t('Name')}</td>
      <td>{t('Role')}</td>
      <td>{t('Organization')}</td>
      <td>{t('Phone')}</td>
      <td>{t('Email')}</td>
    </tr>
  )
}

/**
 * Renders a contact of the contacts list table
 * @selector `.ContactsTableRow`
 * @returns {external:react/Component} The contact information
 */
export const Contact = ({ contacts }) => {
  return (
    <>
      {contacts.map(contact => {
        return (
          <tr className='ContactsTableRow' key={uuidv4()}>
            <td className='ContactFullName'>{contact.fullName}</td>
            <td className='ContactRole'>{contact.role}</td>
            <td className='ContactOrganization'>{contact.organization}</td>
            <td className='ContactPhone'>{contact.phone}</td>
            <td className='ContactEmail'>{contact.email}</td>
          </tr>
        )
      })}
    </>
  )
}

/**
 * Renders the contacts table
 * @selector `.ContactsTable`
 * @returns {external:react/Component} The contacts table
 */
export const ContactsTable = ({ contacts }) => {
  return (
    <table className='ContactsTable'>
      <tbody>
        <ContactsTableHeader />
        <Contact contacts={contacts} />
      </tbody>
    </table>
  )
}

/**
 * Renders the contacts list page of the welcome hub
 * @selector `.ContactsPage`
 * @returns {external:react/Component} The contacts list page
 */
const ContactsPage = () => {
  const cn = classNames('ContactsPage', 'page')
  const { contactsStore } = useContext(AppStoresContext)

  const addContactHandler = () => {
    console.log('I am adding a new contact')
  }

  return (
    <section className={cn}>
      <div className='Pages'>
        <ContactsPageHeader />
        <ContactsTable contacts={contactsStore.availableContacts} />
        <ItemCollectionAdder onClick={addContactHandler} />
      </div>
    </section>
  )
}

export default ContactsPage
