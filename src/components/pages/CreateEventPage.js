import React, { useContext } from 'react'
import ItemCollection from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'

import '@styles/HomePage.scss'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'

/**
 * Renders the presets list page of the welcome hub
 * @selector `.CreateEventPage`
 * @returns {external:react/Component} The presets list page
 */
const CreateEventPage = () => {
  const cn = classNames('CreateEventPage', 'page')
  const { t } = useTranslation()
  const { itemStore } = useContext(AppStoresContext)

  return (
    <section className={cn}>
      <div className='Pages'>
        <div className='ListHeader'>
          <div className='ListHeaderTitle'>
            <h4>{t('Create an event')}</h4>
            <span>{t('Here are different scenography models allowing you to configure your event. Select the one that corresponds to your need to access the creation parameters')}</span>
          </div>
        </div>
        <ItemCollection items={itemStore.presetsItems} onNew={() => {}} />
      </div>
    </section>
  )
}

export default CreateEventPage
