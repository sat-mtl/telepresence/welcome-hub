import React, { useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'
import { Common } from '@sat-mtl/ui-components'
import classNames from 'classnames'

import { AppStoresContext } from '@components/App'
import SceneSchemaLocation from '@components/SceneSchemaLocation'
import { EventStatusEnum } from '@models/EventStatusEnum'
import { formatDateValue } from '@utils/helperFunctions'

import '@styles/HomePage.scss'
import '@styles/EventsPage.scss'

const { Button, Tag } = Common

/**
 * Renders the header of the events page
 * @selector `.PageHeader`
 * @returns {external:react/Component} The events page header
 */
export const EventsPageHeader = () => {
  const { t } = useTranslation()

  return (
    <div className='PageHeader'>
      <h4>{t('My events')}</h4>
    </div>
  )
}

/**
 * Renders the header of the events list table
 * @selector `.EventsTableHeader`
 * @returns {external:react/Component} The events table header
 */
export const EventsTableHeader = () => {
  const { t } = useTranslation()
  const headerItems = ['Status', 'Name', 'Preset', 'Date', 'Hour', 'Partners Halls']

  return (
    <tr className='EventsTableHeader'>
      {headerItems.map(item => <td key={uuidv4()}>{t(item)}</td>)}
    </tr>
  )
}

/**
 * Renders the button container for the event halls item
 * @selector `.ButtonsContainer`
 * @param {string} eventStatus - The status of the event
 * @returns {external:react/Component} The event button container
 */
export const EventButtonContainer = ({ eventStatus }) => {
  const { t } = useTranslation()
  return (
    <div className='ButtonsContainer'>
      <Button
        variant={eventStatus === EventStatusEnum.PLANNING ? 'contained' : 'outlined'}
        size='small'
        onClick={() => console.log('I should be displaying the preset scene')}
      >
        {eventStatus === EventStatusEnum.PLANNING ? t('Access') : t('Show')}
      </Button>
    </div>
  )
}

/**
 * Renders the event status tag
 * @selector `.EventStatusTag`
 * @param {string} status - The status of the event
 * @returns {external:react/Component} The event status tag
 */
export const EventStatusTag = ({ status }) => {
  const { t } = useTranslation()
  const { eventsStore } = useContext(AppStoresContext)
  return (
    <Tag className='EventStatusTag' color={eventsStore.getEventStatusTagColor(status)}>
      {t(status)}
    </Tag>
  )
}

/**
 * Renders the event halls container
 * @selector `.HallsContainer`
 * @param {object} halls - An array with all the participating halls
 * @param {object} halls - An array with all halls colors
 * @returns {external:react/Component} The event halls container
 */
export const EventHallsContainer = ({ halls, colors }) => {
  return (
    <div className='HallsContainer'>
      {halls.map((hall, index) => {
        return <SceneSchemaLocation key={uuidv4()} location={hall} color={colors[index]} />
      })}
    </div>
  )
}

/**
 * Renders an event of the events list table
 * @selector `.EventsTableRow`
 * @param {object} events - An array with all the events
 * @returns {external:react/Component} The event information
 * @todo the access button should open the first page in `preset-onboarding`- to be implemented
 */
export const EventRow = ({ events }) => {
  const { t } = useTranslation()

  return (
    <>
      {events.map(event => {
        const { status, name, presetTitle, date, hour, halls, hallsNumber, colors } = event
        const eventStatus = status.toLowerCase()

        return (
          <tr className='EventsTableRow' key={uuidv4()} valign='top'>
            <td className='EventStatus'><EventStatusTag status={status} /></td>
            <td className='EventName'>{t(name)}</td>
            <td className='EventPreset'>{`${hallsNumber} ${t('Halls')} - `}<span className='EventPresetTitle'>{t(presetTitle)}</span></td>
            <td className='EventDate'>{formatDateValue(date)}</td>
            <td className='EventHour'>{hour}</td>
            <td className='EventHalls'>
              <EventHallsContainer halls={halls} colors={colors} />
              <EventButtonContainer eventStatus={eventStatus} />
            </td>
          </tr>
        )
      })}
    </>
  )
}

/**
 * Renders the events table
 * @param {object} events - An array with all the events
 * @selector `.EventsTable`
 * @returns {external:react/Component} The event table
 */
export const EventsTable = ({ events }) => {
  return (
    <table className='EventsTable'>
      <tbody>
        <EventsTableHeader />
        <EventRow events={events} />
      </tbody>
    </table>
  )
}

/**
 * Renders the events list page of the welcome hub
 * @selector `.EventsPage`
 * @param {object} events - An array with all the events
 * @returns {external:react/Component} The events list page
 */
const EventsPage = ({ events }) => {
  const cn = classNames('EventsPage', 'page')

  return (
    <section className={cn}>
      <div className='Pages'>
        <EventsPageHeader />
        <EventsTable events={events} />
      </div>
    </section>
  )
}

export default EventsPage
