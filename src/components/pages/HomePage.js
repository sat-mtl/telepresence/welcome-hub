import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { Common } from '@sat-mtl/ui-components'

import ItemCollection from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'

import { RouteEnum } from '@models/RouteEnum'

import '@styles/HomePage.scss'
import { useTranslation } from 'react-i18next'
import EventsPage from './EventsPage'
import classNames from 'classnames'

const { Button } = Common

export const DisplayedItemCollection = [{
  pageTitle: 'Presets',
  routePath: RouteEnum.CREATE_EVENT
}, {
  pageTitle: 'My events',
  routePath: RouteEnum.EVENTS
}, {
  pageTitle: 'Training',
  routePath: RouteEnum.TRAINING
}]

/**
 * Renders the header of the home page for the welcome hub
 * @selector `.listHeader`
 * @param {object} item - A displayed collection item
 * @returns {external:react/Component} The header of the home page
 */
export const HomePageCollectionHeader = ({ item }) => {
  const { t } = useTranslation()
  const { itemStore } = useContext(AppStoresContext)
  return (
    <div className='ListHeader'>
      <h4>{t(item.pageTitle)}
        <span className='PageCounter'>
          ({itemStore.populateDisplayedItemsCount(item.routePath)}/{itemStore.populateItemsCount(item.routePath)})
        </span>
      </h4>
      <Link to={item.routePath}>
        <Button variant='outlined' size='small' className='ShowAllButton'>{t('Show all')}</Button>
      </Link>
    </div>
  )
}

/**
 * Renders the respective item collection of the home page
 * @param {object} item - A displayed collection item
 * @returns {external:react/Component} The respective item collection
 */
export const HomePageItemCollection = ({ item }) => {
  const { itemStore, eventsStore } = useContext(AppStoresContext)
  return (
    <>
      {item.routePath === RouteEnum.PRESETS && <ItemCollection items={itemStore.displayedPresets} />}
      {item.routePath === RouteEnum.EVENTS && <EventsPage events={eventsStore.displayedEvents} />}
      {item.routePath === RouteEnum.CREATE_EVENT && <ItemCollection items={itemStore.displayedPresets} />}
      {item.routePath === RouteEnum.TRAINING && <ItemCollection items={itemStore.displayedTraining} />}
    </>
  )
}

/**
 * Renders the home page of the welcome hub
 * @selector `.HomePage`
 * @returns {external:react/Component} The home page of the application
 */
const HomePage = () => {
  const cn = classNames('HomePage', 'page')
  return (
    <section className={cn}>
      {DisplayedItemCollection.map(item => {
        return (
          <div key={item.pageTitle} className='Pages'>
            <HomePageCollectionHeader item={item} />
            <HomePageItemCollection item={item} />
          </div>
        )
      })}
    </section>
  )
}

export default HomePage
