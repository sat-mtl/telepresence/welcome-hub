import React, { useContext } from 'react'

import ItemCollection from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'

import '@styles/HomePage.scss'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'

/**
 * Renders the training list page of the welcome hub
 * @selector `.TrainingPage`
 * @returns {external:react/Component} The Training list page
 */
const TrainingPage = () => {
  const cn = classNames('TrainingPage', 'page')
  const { t } = useTranslation()
  const { itemStore } = useContext(AppStoresContext)

  return (
    <section className={cn}>
      <div className='Pages'>
        <div className='ListHeader'>
          <h4>{t('Training')}</h4>
        </div>
        <ItemCollection items={itemStore.trainingItems} onNew={() => {}} />
      </div>
    </section>
  )
}

export default TrainingPage
