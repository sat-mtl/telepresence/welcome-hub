/**
 * @classdesc Model used to create a new contact collection model from a JSON object
 * @memberof models
 */
class Contact {
  constructor (fullName, role, organization, phone, email) {
    if (typeof fullName === 'undefined' || typeof fullName !== 'string') {
      throw new TypeError('Attribute `name` is required and must be a string')
    }

    if (typeof role !== 'string') {
      throw new TypeError('Attribute `role` must be a string')
    }

    if (typeof organization !== 'string') {
      throw new TypeError('Attribute `organization` must be a string')
    }

    if (typeof phone !== 'string') {
      throw new TypeError('Attribute `phone` must be a string')
    }

    if (typeof email !== 'string') {
      throw new TypeError('Attribute `email` must be a string')
    }

    this.fullName = fullName
    this.role = role
    this.organization = organization
    this.phone = phone
    this.email = email
  }

  /**
   * Parses a JSON element to a new contact model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let contact = null

    try {
      const { fullName, role, organization, phone, email } = json
      contact = new Contact(fullName, role, organization, phone, email)
    } catch {
      throw new Error('Failed to make a contact from json object')
    }

    return contact
  }

  /**
   * Parses multiple JSON elements to new contact models from a json array
   * @param {Object} jsonArray - A JSON array
   * @static
   */
  static fromArray (jsonArray) {
    return jsonArray.map(json => Contact.fromJSON(json))
  }
}

export default Contact
