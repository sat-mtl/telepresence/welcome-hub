
/**
 * Enum for all create event states
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const CreateEventEnum = Object.freeze({
  BROWSING: 'browsing',
  CREATING: 'creating',
  INVITING: 'inviting'
})
