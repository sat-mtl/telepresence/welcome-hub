/**
 * @classdesc Model used to create a new event model from a JSON object
 * @memberof models
 */
class Event {
  /**
   * Instantiates a new event model
   * @param {string} status - The status of the event
   * @param {string} name - The name of the event
   * @param {string} presetTitle - The title of the preset
   * @param {string} date - The date of the event
   * @param {string} hour - The hour of the event
   * @param {object} halls - An array of all event halls
   * @param {string} hallsNumber - The number of event halls
   * @param {object} colors - An array of halls colors
   * @constructor
   */
  constructor (status, name, presetTitle, date, hour, halls, hallsNumber, colors) {
    if (typeof status === 'undefined' || typeof status !== 'string') {
      throw new TypeError('Attribute `status` is required and must be a string')
    }

    if (typeof name === 'undefined' || typeof name !== 'string') {
      throw new TypeError('Attribute `name` is required and must be a string')
    }

    if (typeof presetTitle === 'undefined' || typeof presetTitle !== 'string') {
      throw new TypeError('Attribute `presetTitle` is required and must be a string')
    }

    if (typeof date === 'undefined' || typeof date !== 'string') {
      throw new TypeError('Attribute `date` is required and must be a string')
    }

    if (typeof hour === 'undefined' || typeof hour !== 'string') {
      throw new TypeError('Attribute `hour` is required and must be a string')
    }

    if (typeof halls === 'undefined' || typeof halls !== 'object') {
      throw new TypeError('Attribute `halls` is required and must be an array')
    }

    if (typeof hallsNumber === 'undefined' || typeof hallsNumber !== 'string') {
      throw new TypeError('Attribute `hallsNumber` must be a string')
    }

    if (typeof colors === 'undefined' || typeof colors !== 'object') {
      throw new TypeError('Attribute `colors` is required and must be an array')
    }

    this.status = status
    this.name = name
    this.presetTitle = presetTitle
    this.date = date
    this.hour = hour
    this.halls = halls
    this.hallsNumber = hallsNumber
    this.colors = colors
  }

  /**
   * Parses a JSON element to a new event model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let event = null

    try {
      const { status, name, presetTitle, date, hour, halls, hallsNumber, colors } = json
      event = new Event(status, name, presetTitle, date, hour, halls, hallsNumber, colors)
    } catch {
      throw new Error('Failed to make an event from json object')
    }

    return event
  }

  /**
   * Parses multiple JSON elements to new event models from a json array
   * @param {Object} jsonArray - A JSON array
   * @static
   */
  static fromArray (jsonArray) {
    return jsonArray.map(json => Event.fromJSON(json))
  }
}

export default Event
