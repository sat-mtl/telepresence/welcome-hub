/**
 * Enum for all event statuses
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const EventStatusEnum = Object.freeze({
  PLANNING: 'planning',
  CANCELED: 'canceled',
  ARCHIVED: 'archived'
})
