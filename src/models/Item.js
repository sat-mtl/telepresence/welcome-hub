
export const DISPLAYED_ITEMS_COUNT = 3

/**
 * @classdesc Model used to create a new item collection model from a JSON object
 * @memberof models
 */
class Item {
  constructor (id, preview = null, title, date = null, type = null, description, halls = [], locations = null, tags = [], buttonLabel = '', detailsLabel = '') {
    if (typeof id === 'undefined') {
      throw new TypeError('Attribute `id` is required')
    }

    if (typeof title === 'undefined' || typeof title !== 'string') {
      throw new TypeError('Attribute `title` is required and must be a string')
    }

    if (typeof description === 'undefined' || typeof description !== 'string') {
      throw new TypeError('Attribute `description` is required and must be a string')
    }

    if (!Array.isArray(tags)) {
      throw new TypeError('tags must be an array')
    }

    if (!Array.isArray(halls)) {
      throw new TypeError('halls must be an array')
    }

    this.id = id
    this.preview = preview
    this.title = title
    this.date = date
    this.type = type
    this.description = description
    this.tags = tags
    this.halls = halls
    this.locations = locations
    this.buttonLabel = buttonLabel
    this.detailsLabel = detailsLabel
  }

  /**
   * Parses a JSON element to a new item model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let item = null

    try {
      const { id, preview, title, date, type, description, halls, locations, tags, buttonLabel, detailsLabel } = json
      item = new Item(id, preview, title, date, type, description, halls, locations, tags, buttonLabel, detailsLabel)
    } catch {
      throw new Error('Failed to make an item from json object')
    }

    return item
  }

  /**
   * Parses multiple JSON elements to new item models from a json array
   * @param {Object} jsonArray - A JSON array
   * @static
   */
  static fromArray (jsonArray) {
    return jsonArray.map(json => Item.fromJSON(json))
  }
}

export default Item
