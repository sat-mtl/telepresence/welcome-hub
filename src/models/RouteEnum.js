/**
 * Enum for all pages routes
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const RouteEnum = Object.freeze({
  HOME: '/',
  CREATE_EVENT: '/create-event-page',
  CREATE_EVENT_FORM: '/create-event-form',
  INVITE_PARTNERS_FORM: '/invite-partners-form',
  EVENTS: '/events-page',
  TRAINING: '/training-page',
  CONTACTS: '/contacts-page'
})
