import ItemStore from '@stores/ItemStore'
import ContactsStore from '@stores/ContactsStore'
import EventsStore from '@stores/EventsStore'
import CreateEventCycleStore from '@stores/CreateEventCycleStore'

/**
 * Creates all the app's Stores
 * @returns {object} Object with all stores used by the Welcome Hub app
 */
function populateStores () {
  const createEventCycleStore = new CreateEventCycleStore()
  const contactsStore = new ContactsStore()
  const eventsStore = new EventsStore()
  const itemStore = new ItemStore(eventsStore)

  return {
    createEventCycleStore,
    contactsStore,
    eventsStore,
    itemStore
  }
}

export default populateStores
