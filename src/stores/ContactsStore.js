import Contact from '@models/Contact'
import JsonItems from '@assets/json/contacts.json'

/**
 * @classdesc Stores all contact lists
 * @memberof stores
 */
class ContactsStore {
  /**
   * Gets the available contacts
   * @todo Get real data from a DB through an API call
   */
  get availableContacts () {
    let contacts = []
    contacts = this.makeContactsModel(JsonItems)

    return contacts
  }

  /**
  * Creates a page contacts model from a JSON object
  * @param {Object} json - JSON representation of the page contacts list
  * @returns {module:models/preset} A page contacts model
  */
  makeContactsModel (json) {
    let collectionItems = null
    try {
      collectionItems = Contact.fromArray(json)
    } catch {
      throw new Error(`Failed to make a contacts model from ${json}`)
    }
    return collectionItems
  }
}

export default ContactsStore
