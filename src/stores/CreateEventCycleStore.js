import { makeObservable, observable, action } from 'mobx'
import { CreateEventEnum } from '@models/CreateEventCycleEnum'

/**
 * @classdesc Stores all cycles
 * @memberof stores
 */
class CreateEventCycleStore {
  /** @property {string} currentCycle - The current cycle of the create event */
  currentCycle = CreateEventEnum.BROWSING

  /** @property {boolean} isCreating - Flags true if the user is creating an event */
  get isCreating () {
    return this.currentCycle === CreateEventEnum.CREATING
  }

  /** @property {boolean} isInviting - Flags true if the user is inviting partners to an event */
  get isInviting () {
    return this.currentCycle === CreateEventEnum.INVITING
  }

  constructor () {
    makeObservable(this, {
      currentCycle: observable,
      setCurrentCycle: action
    })
  }

  /**
  * Sets the current cycle of the app
  * @param {string} value - The current cycle value
  */
  setCurrentCycle (value) {
    this.currentCycle = value
  }
}

export default CreateEventCycleStore
