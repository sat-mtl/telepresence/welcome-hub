import Event from '@models/Event'
import JsonItems from '@assets/json/events.json'
import { EventStatusEnum } from '@models/EventStatusEnum'
import { DISPLAYED_ITEMS_COUNT } from '@models/Item'

/**
 * @classdesc Stores all contact lists
 * @memberof stores
 */
class EventsStore {
  /**
   * Gets the available events
   * @returns {object} - An array of all available events
   * @todo Get real data from a DB through an API call
   */
  get availableEvents () {
    let events = []
    events = this.makeEventModel(JsonItems)

    return events
  }

  /**
   * Gets the events that should be displayed in the home page
   * @returns {object} - An array of all displayed events
   */
  get displayedEvents () {
    return this.availableEvents.slice(-DISPLAYED_ITEMS_COUNT)
  }

  /**
   * Gets the amount of events that should be displayed in the home page
   * @returns {number} - The number of event items
   */
  get eventsItemCount () {
    return this.availableEvents.length
  }

  /**
  * Makes the color of the event status tag
  * @param {string} status - The event status
  * @returns {string} The color of the event status tag
  */
  getEventStatusTagColor (status) {
    let color = ''

    switch (status.toLowerCase()) {
      case EventStatusEnum.PLANNING:
        color = '#2F8813'
        break

      case EventStatusEnum.CANCELED:
        color = '#BE3C3A'
        break

      case EventStatusEnum.ARCHIVED:
        color = '#494949'
        break

      default:
        color = '#C7C7C7'
        break
    }

    return color
  }

  /**
  * Creates a page events model from a JSON object
  * @param {Object} json - JSON representation of the page events list
  * @returns {module:models/preset} A page events model
  */
  makeEventModel (json) {
    let eventItems = null
    try {
      eventItems = Event.fromArray(json)
    } catch {
      throw new Error(`Failed to make a events model from ${json}`)
    }
    return eventItems
  }
}

export default EventsStore
