import Item, { DISPLAYED_ITEMS_COUNT } from '@models/Item'
import { makeObservable, observable, action } from 'mobx'

import trainingItems from '@assets/json/training.json'
import presetsItems from '@assets/json/presets.json'

import EventsStore from '@stores/EventsStore'

import { RouteEnum } from '@models/RouteEnum'

/**
 * @classdesc Stores all item lists
 * @memberof stores
 */
class ItemStore {
  /** @property {string} currentItemId - The id of the current preset item */
  currentItemId = ''

  constructor (eventsStore) {
    makeObservable(this, {
      currentItemId: observable,
      setCurrentItemId: action
    }
    )
    if (eventsStore instanceof EventsStore) {
      this.eventsStore = eventsStore
    } else {
      throw new TypeError('EventsStore is required')
    }
  }

  /**
   * Gets the items of the presets collection
   * @todo Get real data from a DB through an API call
   */
  get presetsItems () {
    let items = []
    items = this.makeItemsModel(presetsItems)
    return items
  }

  /**
   * Gets the items of the training collection
   * @todo Get real data from a DB through an API call
   */
  get trainingItems () {
    let items = []
    items = this.makeItemsModel(trainingItems)

    return items
  }

  /**
   * Gets the total number of items of the presets collection
   */
  get presetItemsCount () {
    return this.presetsItems.length
  }

  /**
   * Gets the total number of items of the training collection
   */
  get trainingItemsCount () {
    return this.trainingItems.length
  }

  /**
   * Gets the event items that should be displayed in the home page preview
   */
  get displayedPresets () {
    return this.presetsItems.slice(-DISPLAYED_ITEMS_COUNT)
  }

  /**
   * Gets the training items that should be displayed in the home page preview
   */
  get displayedTraining () {
    return this.trainingItems.slice(-DISPLAYED_ITEMS_COUNT)
  }

  /**
   * Gets the current preset item object
   */
  get currentPresetItem () {
    return this.presetsItems?.find(preset => preset.id === this.currentItemId)
  }

  /**
   * Gets the total number of items of the routed items collection
   */
  populateItemsCount (routePath) {
    switch (routePath) {
      case (RouteEnum.CREATE_EVENT):
        return this.presetItemsCount

      case (RouteEnum.EVENTS):
        return this.eventsStore.eventsItemCount

      case (RouteEnum.TRAINING):
        return this.trainingItemsCount

      default:
        return ''
    }
  }

  /**
   * Gets the total number of items of the routed items collection
   */
  populateDisplayedItemsCount (routePath) {
    switch (routePath) {
      case (RouteEnum.CREATE_EVENT):
        return this.displayedPresets.length

      case (RouteEnum.EVENTS):
        return this.eventsStore.displayedEvents.length

      case (RouteEnum.TRAINING):
        return this.displayedTraining.length

      default:
        return ''
    }
  }

  /**
  * Creates a page items model from a JSON object
  * @param {Object} json - JSON representation of the page items list
  * @returns {module:models/preset} A page items model
  */
  makeItemsModel (json) {
    let collectionItems = null
    try {
      collectionItems = Item.fromArray(json)
    } catch {
      throw new Error(`Failed to make an items model from ${json}`)
    }
    return collectionItems
  }

  /**
  * Sets the id for the selected preset item
  * @param {string} value - The id of the current preset item
  */
  setCurrentItemId (value) {
    this.currentItemId = value
  }
}

export default ItemStore
