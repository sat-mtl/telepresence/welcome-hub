import React from 'react'
import { createUseStyles } from 'react-jss'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import '@styles/InvitePartnerFormContainer.scss'

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  FormFields: {
    border: props => `1px solid ${props.color}`

  },
  HallNumber: {
    backgroundColor: props => props.color
  }
})

/**
 * Renders a container of required fields and the hall title
 * @selector`.FormContentContainer`
 * @param {object} props - Props that are passed to the react component
 * @returns {external:react/Component} The Invite partners fields container
 */
const InvitePartnerFormContainer = (props) => {
  const { t } = useTranslation()
  const classes = useStyles(props)
  return (
    <div className='FormContentContainer'>
      <span className='HallTitle'>
        {t(props.title.split(' ')[0])}
        <span className={`${classes.HallNumber} HallNumber`}>
          {props.hallNumber}
        </span>
      </span>
      <div className={`${classes.FormFields} FormFields`}>
        {props.children}
      </div>
    </div>

  )
}

InvitePartnerFormContainer.propTypes = {
  // The title that is displayed on the left of the container
  title: PropTypes.string,
  // The number of the hall that for the respective container
  hallNumber: PropTypes.number,
  // The color of the container's border and the background of the hall number
  color: PropTypes.string
}

InvitePartnerFormContainer.defaultProps = {
  title: 'hall',
  color: '#FDD153'
}

export default InvitePartnerFormContainer
