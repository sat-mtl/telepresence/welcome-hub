import { useTranslation } from 'react-i18next'

/**
 * Checks if all boolean properties following a specific regex pattern are valid
 * @param {Object} object - The object containing all the fields to check
 * @param {string} regexPattern - The regex pattern
 * @returns {boolean} - Flags true if all boolean fields are true
 */
export const areAllBoolFieldsValid = (object, regexPattern) => {
  return Object.entries(object).filter(arr => regexPattern.test(arr[0])).every(arr => arr[1])
}

/**
 * Converts a timestamp to a formatted date that can be translated
 * @param {string} timestamp - A timestamp value
 * @returns {string} - The formatted date
 */
export const formatDateValue = function (date) {
  const { t } = useTranslation()
  return t('intlDateTime',
    {
      val: new Date(+date),
      formatParams: {
        val: { year: 'numeric', month: 'long', day: 'numeric' }
      }
    })
}
