/* global describe it expect */

import React from 'react'
import { mount } from 'enzyme'
import { MemoryRouter } from 'react-router'

import { DISPLAYED_ITEMS_COUNT } from '@models/Item'
import HomePage from '@components/pages/HomePage'
import TrainingPage from '@components/pages/TrainingPage'
import ContactsPage from '@components/pages/ContactsPage'
import CreateEventPage from '@components/pages/CreateEventPage'
import EventsPage from '@components/pages/EventsPage'
import App from '@components/App'

import { RouteEnum } from '@models/RouteEnum'

describe('<App />', () => {
  it('should display the home page when the relative path is set to it', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.HOME]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(HomePage)).toHaveLength(1)
    expect(wrapper.find(CreateEventPage)).toHaveLength(0)
    expect(wrapper.find(TrainingPage)).toHaveLength(0)
    expect(wrapper.find(ContactsPage)).toHaveLength(0)
  })

  it('should display the set amount of events in the events preview', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.HOME]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(EventsPage)).toHaveLength(1)
    expect(wrapper.find(EventsPage).find('.EventsTableRow')).toHaveLength(DISPLAYED_ITEMS_COUNT)
  })

  it('should display the create event page when the relative path is set to it', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.CREATE_EVENT]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(CreateEventPage)).toHaveLength(1)
    expect(wrapper.find(HomePage)).toHaveLength(0)
    expect(wrapper.find(TrainingPage)).toHaveLength(0)
    expect(wrapper.find(EventsPage)).toHaveLength(0)
    expect(wrapper.find(ContactsPage)).toHaveLength(0)
  })

  it('should display the events page when the relative path is set to it', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.EVENTS]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(EventsPage)).toHaveLength(1)
    expect(wrapper.find(HomePage)).toHaveLength(0)
    expect(wrapper.find(TrainingPage)).toHaveLength(0)
    expect(wrapper.find(CreateEventPage)).toHaveLength(0)
    expect(wrapper.find(ContactsPage)).toHaveLength(0)
  })

  it('should display the training page when the relative path is set to it', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.TRAINING]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(TrainingPage)).toHaveLength(1)
    expect(wrapper.find(HomePage)).toHaveLength(0)
    expect(wrapper.find(EventsPage)).toHaveLength(0)
    expect(wrapper.find(CreateEventPage)).toHaveLength(0)
    expect(wrapper.find(ContactsPage)).toHaveLength(0)
  })

  it('should display the contacts page when the relative path is set to it', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={[RouteEnum.CONTACTS]}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(ContactsPage)).toHaveLength(1)
    expect(wrapper.find(HomePage)).toHaveLength(0)
    expect(wrapper.find(EventsPage)).toHaveLength(0)
    expect(wrapper.find(CreateEventPage)).toHaveLength(0)
    expect(wrapper.find(TrainingPage)).toHaveLength(0)
  })

  it('should display the home page when the path is not one that is included in RouteEnum', () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={['/something']}>
        <App />
      </MemoryRouter>
    )
    expect(wrapper.find(HomePage)).toHaveLength(1)
    expect(wrapper.find(ContactsPage)).toHaveLength(0)
    expect(wrapper.find(CreateEventPage)).toHaveLength(0)
    expect(wrapper.find(TrainingPage)).toHaveLength(0)
    // only with set amount of displayed events
    expect(wrapper.find(EventsPage).find('.EventsTableRow')).toHaveLength(DISPLAYED_ITEMS_COUNT)
  })
})
