/* global describe it expect beforeEach jest */

import React from 'react'
import { mount } from 'enzyme'
import { Common } from '@sat-mtl/ui-components'

import ItemCollection, { Item as CollectionItem, ItemCollectionAdder } from '@components/ItemCollection'
import Item from '@models/Item'
import json from '~/assets/json/presets.json'

import populateStores from '~/src/populateStores'
import { AppStoresContext } from '@components/App'
import { MemoryRouter } from 'react-router-dom'

const { Button } = Common

describe('<ItemCollection />', () => {
  let onNew, itemStore
  beforeEach(() => {
    ({ itemStore } = populateStores())
    onNew = jest.fn()
  })

  function mountItemCollection () {
    return mount(
      <MemoryRouter>
        <AppStoresContext.Provider value={{ itemStore }}>
          <ItemCollection items={itemStore.makeItemsModel(Item.fromArray(json))} onNew={onNew} />
        </AppStoresContext.Provider>
      </MemoryRouter>
    )
  }

  function mountItem (item) {
    return mount(
      <MemoryRouter>
        <AppStoresContext.Provider value={{ itemStore }}>
          <CollectionItem item={item} />
        </AppStoresContext.Provider>
      </MemoryRouter>
    )
  }

  function mountItemCollectionAdder () {
    return mount(
      <AppStoresContext.Provider value={{ itemStore }}>
        <ItemCollectionAdder onClick={onNew} />
      </AppStoresContext.Provider>
    )
  }

  it('should display the given items in an unordered list and a button', () => {
    const wrapper = mountItemCollection()

    expect(wrapper.find('ul').length).toEqual(1)
    expect(wrapper.find(CollectionItem).length).toEqual(itemStore.makeItemsModel(Item.fromArray(json)).length)
    expect(wrapper.find('li').length).toEqual(itemStore.makeItemsModel(Item.fromArray(json)).length)
    expect(wrapper.find(ItemCollectionAdder).length).toEqual(1)
  })

  describe('<Item />', () => {
    it('should render a list item with a header and a description', () => {
      const wrapper = mountItem(
        itemStore.makeItemsModel(Item.fromArray(json))[0]
      )

      expect(wrapper.find('li').length).toEqual(1)
      expect(wrapper.find('.Description').length).toEqual(1)
    })
  })

  describe('<ItemCollectionAdder />', () => {
    it('should render a button that calls a function once clicked', () => {
      const wrapper = mountItemCollectionAdder()
      wrapper.find(Button).prop('onClick')()

      expect(wrapper.find(Button).length).toEqual(1)
      // expect(onNew).toHaveBeenCalled()
    })
  })
})
