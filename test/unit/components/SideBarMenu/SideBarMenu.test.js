/* global describe it expect */

import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router } from 'react-router-dom'

import SideBarMenu, { MenuItem } from '@components/SideBarMenu/SideBarMenu.js'
import { MainMenuList, EventsMenuList } from '@components/SideBarMenu/MenuList'

describe('<SideBarMenu />', () => {
  // if not wrapped with Router, it results in the error (useLocation() may be used only in the context of a Router component)
  // This happens when the rendered component in the test or one of its children makes use of the useLocation hook
  function mountSideBarMenu () {
    return mount(
      <Router>
        <SideBarMenu />
      </Router>
    )
  }

  it('should render a nav link, two unordered list and a heading', () => {
    const wrapper = mountSideBarMenu()

    expect(wrapper.find('nav').length).toEqual(1)
    expect(wrapper.find('ul').length).toEqual(2)
    expect(wrapper.find('h4').length).toEqual(1)
  })

  it('should render all items from MainMenuList and EventMenuList', () => {
    const wrapper = mountSideBarMenu()
    expect(wrapper.find(MenuItem).length).toEqual(MainMenuList.length + EventsMenuList.length)
  })
})
