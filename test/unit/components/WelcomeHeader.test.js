/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'
import WelcomeHeader, { PARENT_APP } from '@components/WelcomeHeader.js'

describe('<WelcomeHeader />', () => {
  it('should have a header tag that displays the name of the parent project', () => {
    const wrapper = shallow(<WelcomeHeader />)
    expect(wrapper.find('h3').text()).toEqual(PARENT_APP)
  })
})
