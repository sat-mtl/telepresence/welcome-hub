/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '~/src/populateStores'
import Contact from '~/src/models/Contact'
import { AppStoresContext } from '@components/App'
import ContactsPage, { ContactsPageHeader, ContactsTableHeader, Contact as TableContact, ContactsTable } from '@components/pages/ContactsPage.js'

import JsonItems from '@assets/json/contacts.json'
import { Common } from '@sat-mtl/ui-components'
import { ItemCollectionAdder } from '@components/ItemCollection'

const { Button } = Common

describe('<ContactsPage />', () => {
  let contacts
  let contactsStore
  beforeEach(() => {
    ({ contactsStore } = populateStores())

    contacts = contactsStore.makeContactsModel(Contact.fromArray(JsonItems))
  })

  const InformationFields = Object.keys(JsonItems[0]).length - 1

  it('should render a header, a contacts table, and a add contact button', () => {
    const wrapper = mount(
      <AppStoresContext.Provider value={{ contactsStore }}>
        <ContactsPage />
      </AppStoresContext.Provider>
    )
    const $button = wrapper.find(Button)
    expect(wrapper.find('section').length).toEqual(1)
    expect($button.length).toEqual(1)
    expect(wrapper.find(ContactsPageHeader).length).toEqual(1)
    expect(wrapper.find(ContactsTable).length).toEqual(1)
    expect(wrapper.find(ItemCollectionAdder).length).toEqual(1)
  })

  describe('<ContactsPageHeader />', () => {
    it('should render a header with an h4 heading', () => {
      const wrapper = mount(<ContactsPageHeader />)
      expect(wrapper.find('h4').text()).toEqual('My contacts')
    })
  })

  describe('<ContactsTableHeader />', () => {
    it('should render an html table header with one tr and n td where n is the number of contacts information fields', () => {
      const wrapper = mount(<ContactsTableHeader />)
      expect(wrapper.find('tr').length).toEqual(1)
      expect(wrapper.find('td').length).toEqual(InformationFields)
    })
  })

  describe('<Contact />', () => {
    it('should render a contact row with information ', () => {
      const wrapper = mount(<TableContact contacts={contacts} />)
      expect(wrapper.find('tr').length).toEqual(contacts.length)
      expect(wrapper.find('td').length).toEqual(contacts.length * InformationFields)
    })
  })

  describe('<ContactsTable />', () => {
    it('should render a table html element with a table body ', () => {
      const wrapper = mount(<ContactsTable contacts={contacts} />)
      expect(wrapper.find('table').length).toEqual(1)
      expect(wrapper.find('tbody').length).toEqual(1)
    })
  })
})
