/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '~/src/populateStores'
import CreateEventPage from '@components/pages/CreateEventPage.js'
import ItemCollection from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'

import { Common } from '@sat-mtl/ui-components'
import { MemoryRouter } from 'react-router-dom'
const { Button } = Common

describe('<CreateEventPage />', () => {
  let itemStore
  beforeEach(() => {
    ({ itemStore } = populateStores())
  })

  function mountCreateEventPage () {
    return mount(
      <MemoryRouter>
        <AppStoresContext.Provider value={{ itemStore }}>
          <CreateEventPage />
        </AppStoresContext.Provider>
      </MemoryRouter>
    )
  }

  describe('<CreateEventPage />', () => {
    it('should render a header with a h4 heading and an item collection', () => {
      const wrapper = mountCreateEventPage()
      expect(wrapper.find('h4').text()).toEqual('Create an event')
      expect(wrapper.find(ItemCollection).length).toEqual(1)
    })

    it('should render a button that can add a new preset and one button for each preset creation', () => {
      const $button = mountCreateEventPage().find(Button)
      const createEventButtons = itemStore.presetItemsCount
      expect($button.length).toEqual(1 + createEventButtons)
    })
  })
})
