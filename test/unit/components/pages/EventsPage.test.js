/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '~/src/populateStores'
import EventsPage from '@components/pages/EventsPage.js'
import { AppStoresContext } from '@components/App'

describe('<EventsPage />', () => {
  let itemStore, eventsStore
  beforeEach(() => {
    ({ itemStore, eventsStore } = populateStores())
  })

  function mountEventsPage () {
    return mount(
      <AppStoresContext.Provider value={{ itemStore, eventsStore }}>
        <EventsPage events={eventsStore.availableEvents} />
      </AppStoresContext.Provider>
    )
  }

  describe('<EventsPage />', () => {
    it('should render a header with a h4 heading', () => {
      const wrapper = mountEventsPage()
      expect(wrapper.find('h4').text()).toEqual('My events')
    })
  })
})
