/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter as Router, Link } from 'react-router-dom'

import populateStores from '~/src/populateStores'
import HomePage, { DisplayedItemCollection, HomePageCollectionHeader, HomePageItemCollection } from '@components/pages/HomePage.js'
import { AppStoresContext } from '@components/App'

describe('<HomePage />', () => {
  let itemStore, eventsStore
  beforeEach(() => {
    ({ itemStore, eventsStore } = populateStores())
  })

  function mountHomePage () {
    return mount(
      <AppStoresContext.Provider value={{ itemStore, eventsStore }}>
        <Router>
          <HomePage />
        </Router>
      </AppStoresContext.Provider>
    )
  }

  describe('<HomePage />', () => {
    it('should render all displayed item collections with their respective headers', () => {
      const wrapper = mountHomePage()
      expect(wrapper.find(HomePageCollectionHeader).length).toEqual(DisplayedItemCollection.length)
      expect(wrapper.find(HomePageItemCollection).length).toEqual(DisplayedItemCollection.length)
    })

    it('should render a show all button for each displayed item collection', () => {
      const $button = mountHomePage().findWhere(node => node.type() === 'button' && node.text() === 'Show all')
      expect($button.length).toEqual(DisplayedItemCollection.length)
    })

    it('should route user to the respective path when a show all button is clicked ', () => {
      const wrapper = mountHomePage()
      DisplayedItemCollection.forEach((item, index) => expect(wrapper.find('.ListHeader').find(Link).at(index).props().to).toBe(DisplayedItemCollection[index].routePath))
    })
  })
})
