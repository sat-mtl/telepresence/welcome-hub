/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '~/src/populateStores'
import TrainingPage from '@components/pages/TrainingPage.js'
import ItemCollection from '@components/ItemCollection'
import { AppStoresContext } from '@components/App'

import { Common } from '@sat-mtl/ui-components'
const { Button } = Common

describe('<TrainingPage />', () => {
  let itemStore
  beforeEach(() => {
    ({ itemStore } = populateStores())
  })

  function mountTrainingPage () {
    return mount(
      <AppStoresContext.Provider value={{ itemStore }}>
        <TrainingPage />
      </AppStoresContext.Provider>
    )
  }

  describe('<TrainingPage />', () => {
    it('should render a header with a h4 heading and an item collection', () => {
      const wrapper = mountTrainingPage()
      expect(wrapper.find('h4').text()).toEqual('Training')
      expect(wrapper.find(ItemCollection).length).toEqual(1)
    })

    it('should render a button that can add a new training', () => {
      const $button = mountTrainingPage().find(Button)
      expect($button.length).toEqual(1)
    })
  })
})
