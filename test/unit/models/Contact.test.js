/* global describe it expect jest */

import Contact from '@models/Contact.js'
import contactsItems from '@assets/json/contacts.json'

describe('<Contact/>', () => {
  const NAME = 'test'
  const ROLE = 'test'
  const ORGANIZATION = 'test'
  const PHONE = 'test'
  const EMAIL = 'test'

  const contact = new Contact(...Object.values(contactsItems[0]).slice(1))

  describe('constructor', () => {
    /* eslint-disable no-new */
    it('should throw an error if it is constructed without a fullName', () => {
      expect(() => { new Contact(EMAIL) }).toThrow()
    })

    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Contact(0) }).toThrow()
      expect(() => { new Contact(0, ROLE, ORGANIZATION, PHONE, EMAIL) }).toThrow()
      expect(() => { new Contact(NAME, 0, ORGANIZATION, PHONE, EMAIL) }).toThrow()
      expect(() => { new Contact(NAME, ROLE, 0, PHONE, EMAIL) }).toThrow()
      expect(() => { new Contact(NAME, ROLE, ORGANIZATION, 0, EMAIL) }).toThrow()
      expect(() => { new Contact(NAME, ROLE, ORGANIZATION, PHONE, 0) }).toThrow()
    })
  })

  describe('fromJSON', () => {
    it('should make a new contact model when a valid json object is provided', () => {
      expect(Contact.fromJSON({ fullName: contactsItems[0].fullName, role: contactsItems[0].role, organization: contactsItems[0].organization, phone: contactsItems[0].phone, email: contactsItems[0].email })).toEqual(contact)
    })

    it('should throw an error if the json parameter is not provided ', () => {
      expect(() => Contact.fromJSON()).toThrow()
    })
  })

  describe('fromArray', () => {
    it('should should make a new contact model when an array of json objects is provided', () => {
      Contact.fromJSON = jest.fn()
      Contact.fromArray(contactsItems)
      expect(Contact.fromJSON).toHaveBeenCalled()
    })

    it('should throw an error if the parameter is not provided ', () => {
      expect(() => Contact.fromArray()).toThrow()
    })
  })
})
