/* global describe it expect jest */

import Item from '@models/Item.js'
import presetsItems from '@assets/json/presets.json'

describe('<Item/>', () => {
  const ID = 'test'
  const PREVIEW = 'test'
  const TITLE = 'test'
  const DATE = Date.now()
  const TYPE = 'test'
  const DESCRIPTION = 'test'
  const HALLS = ['test']

  const item = new Item(ID, PREVIEW, TITLE, null, null, DESCRIPTION, HALLS)

  describe('constructor', () => {
    /* eslint-disable no-new */
    it('should throw an error if it is constructed without a title', () => {
      expect(() => { new Item(ID) }).toThrow()
    })

    it('should throw an error if it is constructed without a title and a description', () => {
      expect(() => { new Item(ID, PREVIEW) }).toThrow()
    })

    it('should not throw an error if the date parameter is not provided', () => {
      expect(() => { new Item(ID, PREVIEW, TITLE, undefined, TYPE, DESCRIPTION, HALLS) }).not.toThrow()
    })

    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Item(0) }).toThrow()
      expect(() => { new Item(PREVIEW, undefined, DATE, DESCRIPTION, HALLS) }).toThrow()
      expect(() => { new Item(PREVIEW, TITLE, DATE, undefined, undefined) }).toThrow()
      expect(() => { new Item(PREVIEW, TITLE, DATE, TYPE, undefined) }).toThrow()
    })
  })

  describe('fromJSON', () => {
    it('should should make a new presets model when a valid json object is provided', () => {
      expect(Item.fromJSON({ id: ID, preview: PREVIEW, title: TITLE, description: DESCRIPTION, halls: HALLS })).toEqual(item)
    })
    it('should throw an error if the json parameter is not provided ', () => {
      expect(() => Item.fromJSON()).toThrow()
    })
  })

  describe('fromArray', () => {
    it('should should make a new item model when an array of json objects is provided', () => {
      Item.fromJSON = jest.fn()
      Item.fromArray(presetsItems)
      expect(Item.fromJSON).toHaveBeenCalled()
    })

    it('should throw an error if the parameter is not provided ', () => {
      expect(() => Item.fromArray()).toThrow()
    })
  })
})
