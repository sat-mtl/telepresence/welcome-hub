/* global describe it expect beforeEach jest */

import Contact from '@models/Contact'
import JsonItems from '@assets/json/contacts.json'

import populateStores from '~/src/populateStores'

describe('<ContactStore/>', () => {
  let contactsStore
  beforeEach(() => {
    ({ contactsStore } = populateStores())
  })

  describe('makeContactsModel', () => {
    it('should make a defined contact model when a valid json object is passed', () => {
      contactsStore.makeContactsModel(JsonItems)
      expect(contactsStore.makeContactsModel(JsonItems)).not.toBeUndefined()
    })

    it('should return a contacts model array with the same length of the json object that is passed', () => {
      contactsStore.makeContactsModel(JsonItems)
      expect(contactsStore.makeContactsModel(JsonItems).length).toEqual(JsonItems.length)
    })

    it('should throw an error when an invalid json object is passed', () => {
      expect(() => contactsStore.makeContactsModel('test')).toThrow()
    })

    it('should call the fromArray method of the Contact model when called', () => {
      Contact.fromArray = jest.fn()
      contactsStore.makeContactsModel(JsonItems)
      expect(Contact.fromArray).toHaveBeenCalled()
    })
  })
})
