/* global describe it expect beforeEach jest */

import Item from '@models/Item'
import ItemStore from '@stores/ItemStore'
import EventsStore from '@stores/EventsStore'

import presetsItems from '@assets/json/presets.json'

import { RouteEnum } from '@models/RouteEnum'
import populateStores from '~/src/populateStores'

describe('<ContactStore/>', () => {
  let itemStore
  beforeEach(() => {
    ({ itemStore } = populateStores())
  })

  describe('makeItemsModel', () => {
    it('should make a defined item model when a valid json object is passed', () => {
      itemStore.makeItemsModel(presetsItems)
      expect(itemStore.makeItemsModel(presetsItems)).not.toBeUndefined()
      expect(itemStore.makeItemsModel(presetsItems).length).toEqual(presetsItems.length)
    })

    it('should return a items model array with the same length of the json object that is passed', () => {
      itemStore.makeItemsModel(presetsItems)
      expect(itemStore.makeItemsModel(presetsItems).length).toEqual(presetsItems.length)
    })

    it('should throw an error when an invalid json object is passed', () => {
      expect(() => itemStore.makeItemsModel('test')).toThrow()
    })

    it('should call the fromArray method of the Item model when called', () => {
      Item.fromArray = jest.fn()
      itemStore.makeItemsModel(presetsItems)
      expect(Item.fromArray).toHaveBeenCalled()
    })
  })

  describe('populateItemsCount', () => {
    it('should get the preset items count if the passed route is one to the presets', () => {
      const spy = jest.spyOn(ItemStore.prototype, 'presetItemsCount', 'get').mockImplementation(() => jest.fn())
      itemStore.populateItemsCount(RouteEnum.CREATE_EVENT)
      expect(spy).toHaveBeenCalled()
    })

    it('should get the event items count if the passed route is one to the events', () => {
      const spy = jest.spyOn(EventsStore.prototype, 'eventsItemCount', 'get').mockImplementation(() => jest.fn())
      itemStore.populateItemsCount(RouteEnum.EVENTS)
      expect(spy).toHaveBeenCalled()
    })

    it('should get the training items count if the passed route is one to the training', () => {
      const spy = jest.spyOn(ItemStore.prototype, 'trainingItemsCount', 'get').mockImplementation(() => jest.fn())
      itemStore.populateItemsCount(RouteEnum.TRAINING)
      expect(spy).toHaveBeenCalled()
    })

    it('should not call any of the count getters and return an empty string if none of the cases match', () => {
      const spy1 = jest.spyOn(ItemStore.prototype, 'presetItemsCount', 'get').mockImplementation(() => jest.fn())
      const spy2 = jest.spyOn(EventsStore.prototype, 'eventsItemCount', 'get').mockImplementation(() => jest.fn())
      const spy3 = jest.spyOn(ItemStore.prototype, 'trainingItemsCount', 'get').mockImplementation(() => jest.fn())
      const result = itemStore.populateItemsCount('')
      expect(spy1).not.toHaveBeenCalled()
      expect(spy2).not.toHaveBeenCalled()
      expect(spy3).not.toHaveBeenCalled()
      expect(result).toEqual('')
    })
  })

  describe('populateDisplayedItemsCount', () => {
    it('should get the displayed preset items if the passed route is one to the presets', () => {
      const spy = jest.spyOn(ItemStore.prototype, 'displayedPresets', 'get').mockImplementation(() => jest.fn())
      itemStore.populateDisplayedItemsCount(RouteEnum.CREATE_EVENT)
      expect(spy).toHaveBeenCalled()
    })

    it('should get the displayed event items if the passed route is one to the events', () => {
      const spy = jest.spyOn(EventsStore.prototype, 'displayedEvents', 'get').mockImplementation(() => jest.fn())
      itemStore.populateDisplayedItemsCount(RouteEnum.EVENTS)
      expect(spy).toHaveBeenCalled()
    })

    it('should get the displayed training items if the passed route is one to the training', () => {
      const spy = jest.spyOn(ItemStore.prototype, 'displayedTraining', 'get').mockImplementation(() => jest.fn())
      itemStore.populateDisplayedItemsCount(RouteEnum.TRAINING)
      expect(spy).toHaveBeenCalled()
    })

    it('should not call any of the display getters and return an empty string if none of the cases match', () => {
      const spy1 = jest.spyOn(ItemStore.prototype, 'displayedPresets', 'get').mockImplementation(() => jest.fn())
      const spy2 = jest.spyOn(EventsStore.prototype, 'displayedEvents', 'get').mockImplementation(() => jest.fn())
      const spy3 = jest.spyOn(ItemStore.prototype, 'displayedTraining', 'get').mockImplementation(() => jest.fn())
      const result = itemStore.populateDisplayedItemsCount('')
      expect(spy1).not.toHaveBeenCalled()
      expect(spy2).not.toHaveBeenCalled()
      expect(spy3).not.toHaveBeenCalled()
      expect(result).toEqual('')
    })
  })
})
